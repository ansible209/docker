Role Name
=========

Docker

Requirements
------------


Add string via visudo command: $USER ALL=(ALL) NOPASSWD: ALL | use argument --ask-become-pass 



Role Variables
--------------

Use UPDATE_DOCKER=true if you want update Docker Engine 

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

**---
- name: Instal and update Docker
  hosts: all
  become: true

  roles:
  - role: docker**

License
-------

BSD

To do list
------------------

- Tests
- Other OS